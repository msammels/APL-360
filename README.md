# APL\360

[[_TOC_]]

---

![](assets/APLSC_Ken_Iverson-728x1024.jpg){width=25%}

*Ken Iverson at Harvard University*

---

Thousands of programming languages were invented in the first 50 years of the
age of computing. Many of them were similar, and many followed a traditional,
evolutionary path from their predecessors.

But some revolutionary languages had a slant that differentiated them from
their more general-purpose brethren. LISP was for list processing. SNOBOL was
for string manipulation. SIMSCRIPT was for simulation. And APL was for
mathematics, with an emphasis on array processing.

What eventually became APL was first invented by Harvard professor Kenneth
E. Iverson in 1957 as a mathematical notation, not as a computer programming
language. Although other matrix-oriented symbol systems existed, including the
concise tensor notation invented by Einstein, they were oriented more towards
mathematical analysis and less towards synthesis of algorithms. Iverson, who
was a student of Howard Aiken’s, taught what became known as “Iverson
Notation” to his Harvard students to explain algorithms.

Iverson was hired by IBM in 1960 to work with Adin Falkoff and others on his
notation. In his now famous 1962 book “A Programming Language” [^1], he says
the notation is for the description of “procedures…called algorithms or
programs”, and that it is a language because it “exhibits considerable
syntactic structure”. But at that point it was just a notation for people to
read, not a language for programming computers. The book gives many examples
of its use both as a descriptive tool (such as for documenting the definition
of computer instruction sets) and as a means for expressing general algorithms
(such as for sorting and searching). Anticipating resistance to something so
novel, he says in the preface, “It is the central thesis of this book that the
descriptive and analytical power of an adequate programming language amply
repays the considerable effort required for its mastery.” Perhaps he was
warning that mastering the language wasn’t trivial. Perhaps he was also
signaling that, in his view, other notational languages were less than
“adequate”.

The team, of course, soon saw that the notation could be turned into a
language for programming computers. That language, which was called APL
starting in 1966, emphasized array manipulation and used unconventional
symbols. It was like no other computer program language that had been
invented.

APL became popular when IBM introduced “APL\360” for their System/360
mainframe computer. Unlike most other languages at the time, APL\360 was also
a complete interactive programming environment. The programmer, sitting at an
electromechanical typewriter linked to a timeshared computer, could type APL
statements and get an immediate response. Programs could be defined, debugged,
run, and saved on a computer that was simultaneously being used by dozens of
other people.

Written entirely in 360 assembly language, this version of APL took control of
the whole machine. It implemented a complete timesharing operating system in
addition to a high-level language.

The text file contains 37,567 lines, which includes code, macros, and global
definitions. The 90 individual files are separated by ‘./ ADD” commands.

[Download APL\360 Source Code](src)

## Creating the APL Programming Language

Iverson’s book “A Programming Language” [^1] uses a graphical notation that
would have been difficult to directly use as a programming language for
computers. He considered it an extension of matrix algebra, and used common
mathematical typographic conventions like subscripts, superscripts, and
distinctions based on the weight or font of characters. Here, for example, is
a program for sorting numbers:

---

![](assets/APLSC_bubble_sort.jpg){width=25%}

---

To linearize the notation for use as a computer programming language typed at
a keyboard, the APL implementers certainly had to give up the use of labeled
arrows for control transfers. But one feature that they were able to retain,
to some extent, was the use of special symbols for primitive functions, as
illustrated in this program that creates Huffman codes:

---

![](assets/APLSC_huffman_codes.jpg){width=25%}

---

APL uses symbols that are closer to standard mathematics than programming. For
example, the symbol for division is ÷, not /. To support the unconventional
symbols, APL\360 used a custom-designed keyboard with special symbols in the
upper case.


---

![](assets/APLSC_Custom-designed-keyboard.jpg){width=25%}

*APL\360 used a custom-designed keyboard*

---

Even so, there were more special characters than could fit on the keyboard, so
some were typed by overstriking two characters. For example, the “grade up”
character ⍋, a primitive operator used for sorting, was created by typing ∆
(shift H), then backspace, then ∣ (shift M). There was no room left for both
upper- and lower-case letters, so APL supported only capital letters.

For printing programs, Iverson and Falkoff got IBM to design a special type
ball for their 1050 and 2741 terminals, which used the IBM Selectric
typewriter mechanism.

---

![](assets/APLSC_terminal.jpg){width=25%}
![](assets/APLSC_golfball.jpg){width=25%}

---

Now programs could be both typed in and printed. Here, for example, is the
printed version a program from the APL Language manual [^2] that computes the
mathematical determinant of a matrix:

---

![](assets/APLSC_matrix.jpg){width=25%}

---

## A taste of APL

APL is a concise high-level programming language that differs from most others
developed in the 1960s in several respects:

**Order of evaluation:** Expressions in APL are evaluated right-to-left, and
there is no hierarchy of function precedence. For example, typing the
expression

$`2×4+3`$

causes the computer to immediately type the resulting value

$`14`$

The value is not, as in many other languages that have operator precedence
[^11]. Of course, parentheses can be used to group a subexpression to change
the evaluation order. The general rule is that the right argument of any
function is the value of the expression to its right.

**Automatic creation of vectors and arrays:** A higher-dimensional structure
is automatically created by evaluating an expression that returns it, and
scalars can be freely mixed. For example,

$`A ← 2 + 1 2 3`$

creates the vector “1 2 3”, add the scalar 2 to it, and creates the variable A
to hold the vector whose value is

$`3 4 5`$

Variables are never declared; they are created automatically and assume the
size and shape of whatever expression is assigned to them.

**A plethora of primitives:** APL has a rich set of built-in functions (and
“operators” that are applied to functions to yield different functions) that
operate on scalar, vectors, arrays, even higher-dimensional objects, and
combinations of them. For example, the expression to sum the numbers in the
vector “A” created above is simply

$`+/A`$

where / is the “reduction” operator that causes the function to the left to be
applied successively to all the elements of the operand to the right. The
expression to compute the average of the numbers in A also uses the primitive
function ρ to determine how many elements there are in A:

$`(+/A) ÷ ρA`$

Here are some tables from the 1970 “APL\360 User’s Manual” [^3] that give a
flavor of the power and sophistication of the built-in APL functions and
operators.

---

![](assets/APLSC_Tables1.jpg){width=25%}
![](assets/APLSC_table2-745x1024.jpgg){width=25%}
![](assets/APLSC_table3.jpg){width=25%}

---

APL encourages you to think differently about programming, and to use
temporary high-dimensional data structures as intermediate values that are
then reduced using the powerful primitives. A famous example is the following
short but complete program to compute all the prime numbers up to R.

$`(~T∊T∘.×T)/T←1↓⍳R`$

| **subexpression** | **meaning**                                                                                                                                      | **value if R is 6**                                                  |
|:-----------------:|:------------------------------------------------------------------------------------------------------------------------------------------------:|:--------------------------------------------------------------------:|
| $`⍳R`$            | Generate a vector of numbers from 1 to R.                                                                                                        | 1 2 3 4 5 6                                                          |
| $`T←1↓`$          | Drop the first element of the vector and assign the rest to the temporary vector T.                                                              | 2 3 4 5 6                                                            |
| $`T∘.×T`$         | Create the multiplication outer product: a table that holds the result of multiplying each element of T by each element of T.                    | 4 6 8 10 12 6 9 12 15 18 8 12 16 20 24 10 15 20 25 30 12 18 24 30 36 |
| $`T∊`$            | Use the "set membership" operator to find which elements of T are in the table.                                                                  | 0 0 1 0 1                                                            |
| ~                 | Negate the result to identify which elements of T are **not** in the table. These are the integers which do not have any multiples in the table. | 1 1 0 1 0                                                            |
| $`()/T`$          | Select the elements of T which we have identified. These are all the primes less than R.                                                         | 2 3 5                                                                |

Note that there are no loops in this program. The power of APL expressions
means that conditional branches and loops are required far less often than in
more traditional programming languages.

APL operators can be used in easy ways for all sorts of computations that
would usually require loops. For example, an expression that computes the
number of elements of the vector X that are greater than 100 is

$`+/X>100`$

It works because $`X>100`$ returns a bit vector of 0’s and 1’s showing which
elements of **X** are greater than 100, and +/ adds up all the bits in that
vector.

But conditional execution and loops are, of course, sometimes needed. In the
light of later developments in structured programming, APL’s only primitive
for control transfer, the “GO TO LINE x” statement $`→`$, is particularly
weak. Here is an example of a function that computes the greatest common
divisor of its two arguments. The last statement creates a loop by branching
to the beginning. In line 2, conditional transfer of control to line 0 causes
the function to exit and return the value last assigned to G.

---

![](assets/APLSC_GCD-300x178.jpg){width=25%}

---

To learn more about the 1960’s APL language, see the “APL Language” reference
manual [^2] and Paul Berry’s 1969 “APL\360 Primer” [^4].

The language has of course evolved over the years, and more recent versions
include control structures such as IF-THEN-ELSE.

## How APL was implemented

The first computer implementation of APL notation was a batch-oriented
language interpreter written in FORTRAN in 1965 for the IBM 7090 mainframe
computer, by Larry Breed at the IBM Research Center in Yorktown Heights NY and
Philip Abrams, then a graduate student at Stanford University.

The first interactive version was written soon after for the 7093 (an
experimental 7090 with virtual memory) by Larry Breed and Roger Moore. It ran
under the TSM timesharing system and was whimsically called “IVSYS”, which
rhymes with “IBSYS”, the name for the standard 7090 operating system. In a
2012 email Breed says,

> IVSYS provided login, logout, immediate execution and function definition;
> it provided workspaces, both active and stored. Implementation of these was
> rudimentary; mostly we used whatever the TSM project offered us for
> login/logout/saving files. We had only a few weeks to use the 7093 before it
> was decommissioned and Roger and I started planning for a standalone system
> on System/360. In those weeks, Ken and his group saw for the first time what
> executable APL would be like.

Another implementation of a subset of the language was done in 1967 for the
IBM 1130 minicomputer.

The first implementation of APL to get widespread use outside of IBM was for
the IBM System/360. Called “APL\360”, it went into service first within IBM in
November 1966. (The notation “APL\360″, since the backslash was the APL
“expansion” operator, also had a hidden meaning: “APL expands the 360″).

Breed says of the time just before,

> This period, early 1966, was the transitional time from Iverson Notation to
> APL. (Indeed, Adin [Falkoff] came up with “APL” in Spring ’66.) Refinement
> and extension of the language and the environment continued for many
> years. There was next to no code added to make a commercial version, just
> paperwork.

By August 1968 APL\360 was available to IBM customers as an unsupported (“Type
III”) program in IBM’s “Contributed Program Library” [^5]. The principal
implementers were Larry Breed, Dick Lathwell, and Roger Moore; others who had
contributed were Adin Falkoff and Luther Woodrum.

---

![](assets/APLSC_iverson_team-1.jpg){width=25%}

*Early APL developers, from left to right: Dick Lathwell, Ken Iverson, Roger
Moore, Adin Falkoff, Phil Abrams, Larry Breed. The photo was taken about
1983.*

---

Because of the dynamic nature of APL variables, APL\360 was implemented as an
interpreter, not as a compiler that generated machine code. Programs were
stored in an internal form called a “codestring” that directly corresponded to
what the user had typed. The interpreter would then examine the codestring as
the program executed, and dynamically allocate and reconfigure variables as
expressions were evaluated.

The first versions of APL\360 took control of the entire machine. It was thus
a combination operating system, file system, timesharing monitor, command
interpreter, and programming language. Given the limited main memory, user
workspaces were swapped out to drum or disk as needed. Performance was
impressive, which Larry Breed attributes, in his clear and succinct
description of the implementation [^6], to the ability to tailor the operating
system to the requirements of the language.

APL\360 was a conversational language that provided fast response and
efficient execution for as many as 50 simultaneous users. Each user had an
“active workspace” that held programs, variables, and the state of suspended
program execution. System commands like “)LOAD”, “)SAVE”, and “)COPY”
maintained the user’s library of stored workspaces. Other system commands
controlled language features; for example, with “)ORIGIN” the programmer could
control whether vectors and arrays are numbered starting with 0 or 1.

APL was the first introduction to interactive timesharing for many in the
generation of programmers who had suffered through batch programming with
punched cards.

# Applications of APL

Even before it was a computer programming language, Iverson Notation was
useful as a language for documenting algorithms for people. The classic
example is the formal definition of the instruction-set architecture of the
new IBM System/360 computer, which was published in an article in the IBM
Systems Journal by Adin Falkoff, Ken Iverson, and Ed Sussenguth in 1965 [^7].

> The description, which is formal rather than verbal, is accomplished by a
> set of programs, interacting through common variables, used in conjunction
> with auxiliary tables… Although the formal description is complete and
> self-contained, text is provided as an aid to initial study.

---

![](assets/APLSC_memory_access_operation.jpg){width=25%}

---

But the text provided in the paper is much more than that. It is a
line-by-line explanation of the formal description, which means that it is
also a demonstration and explanation of APL’s descriptive power.

The notation used the graphical style for control transfers that was in
Iverson’s book. Here, for example, is the description of a memory access
operation.

It was the transition of APL from a notation for publication into an
interactive computer programming language that made it flourish. When the
APL\360 implementation was available, IBM and others stimulated use by
producing diverse applications such as these:

- **Starmap:** A set of APL functions to calculate and plot the positions of
  the stars and planets. [^8][^9] It was written in 1973 by Paul Berry of IBM
  and John Thorstensen, then an astronomy student at Bryn Mawr college, now
  Professor of Physics and Astronomy at Dartmouth College. It uses classical
  solutions to Kepler’s equations for a particular date and time and a series
  of rotations of coordinates to show where the planets and the stars would
  appear in the bowl of the sky.

- **IBGS: Interactive Business Game Simulation:** “A general computer
  management simulation involving decision making and planning in the
  functional areas of production, marketing and finance.”

- **Zeros and Integrals in APL:** “Using both classical methods such as
  Newton’s and Muller’s and recently developed methods such as Jenkins and
  Traub’s, it finds reals zeros of a real function, real and complex zeros of
  a polynomial with real of complex coefficients, and complex zeros of a
  complex function.”

- **Graphpak – Interactive Graphics Package for APL\360:** “…capabilities
  which range from graphics interface support at the lowest level to several
  application areas at higher levels…A plotting component…linear or
  logarithmic…curve-fitting… A descriptive geometry component allows
  definition, scaling, magnification, translation, rotation, and projected
  display of three-dimensional objects.”

- **Graphs and Histograms in APL:** “produces curves and barcharts at a
  typewriter terminal”.

- **APL Coordinate Geometry System:** “solves coordinate geometry problems
  interactively at a terminal…for use by surveyors, civil engineers, urban
  planners…”

- **APL/PDTS – Programming Development Tracking System:** “…to assist managers
  and planners in monitoring performance against plan on programming
  development projects.”

- **MINIPERT:** “A Critical Path Method (CPM) system for Project Management”

- **APL Econometric Planning Language:** “The practicing economist, business
  forecaster or teacher is provided with easy-to-use tools for interactive
  model building and model solving.”

- **APL Financial Planning System:** “lets the financial analyst and planner
  design reports, specify calculation statements, enter and change data, and
  obtain printed reports with immediate turnaround.”

- **APL Text Editor and Composer:** “This program is designed to process text
  interactively at a terminal…Functions are included for entering, revising,
  composing, printing, and storing text…for use by secretaries, scientists,
  engineers, administrators or any others who produce papers, letters, reports
  or specifications.”

Many of these applications emphasized interactivity, which provided a huge
productivity increase compared to the batch-job processing more typical at the
time. In addition, APL allowed applications to be developed much more
quickly. In a 2012 email, Larry Breed noted,

> Across all fields, the speed at which APL programs can be written makes it
> valuable for modeling and prototyping . … One example: Around 1973,
> Continental Can needed an inventory system for its 21 manufacturing
> plants. Their team of FORTRAN programmers had worked for a year, with no
> success in sight. One STSC salesman, in one weekend, built a usable working
> model in APL Plus.

The areas in which APL had the greatest penetration were in scientific,
actuarial, statistical, and financial applications. For details about the
progression of APL in its first 25 years, see the special 1991 issue of the
IBM System Journal [^10] with 12 papers and one essay on the subject.

## APL praise and criticism

APL was not originally designed as a programming language. As Iverson said,

> The initial motive for developing APL was to provide a tool for writing and
> teaching. Although APL has been exploited mostly in commercial programming,
> I continue to believe that its most important use remains to be exploited:
> as a simple, precise, executable notation for the teaching of a wide range
> of subjects.

With so many terse and unusual symbols, APL computer programs, like the
mathematical notation that inspired it, has a conciseness and elegance many
find appealing. APL attracts fanatic adherents. Alan Perlis (the first
recipient of the ACM’s Turing Award, in 1966) was one:

> The sweep of the eye across a single sentence can expose an intricate,
> ingenious and beautiful interplay of operation and control that in other
> programming languages is observable only in several pages of text. One
> begins to appreciate the emergence and significance of style. [^12]

Many find the freedom of expression in APL liberating.

> I used to describe [Pascal] as a ‘fascist programming language’, because it
> is dictatorially rigid. …If Pascal is fascist, APL is anarchist. [^13]

But APL programs are often cryptic and hard to decode. Some have joked that it
is a “write-only language” because even the author of a program might have
trouble understanding it later. It inspires programming trickery. The
challenge of writing an APL “one-liner” to implement a complete complex
algorithm is hard to resist. Here, for example, are two different APL
one-liners that implement versions of John Conway’s “[Game of
Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)“:

---

![](assets/APLSC_apl_life3-1024x38.jpg){width=50%}

---

Not for the faint-hearted, clearly. Dutch computer scientist Edsger Dijkstra
said,

> APL is a mistake, carried through to perfection. It is the language of the
> future for the programming techniques of the past: it creates a new
> generation of coding bums. [^14]

But fans of APL would say that cryptic APL coding is a bad programming style
that can be an affliction with any language. APL provides a richer palette for
expressing algorithms, the argument goes, so you can solve harder problems
faster and with less irrelevant syntactic clutter.

## A short biography of Ken Iverson

---

![](assets/APLSC_ken_iverson1.jpg){width=25%}

---

Kenneth Eugene Iverson was born on December 17, 1920 on a farm near Camrose,
Alberta, Canada. He was educated in rural one-room schools until the end of
9th grade, when he dropped out of school because it was the height of the
Depression and there was work to do on the family farm. He later said the only
purpose of continuing his schooling would have been to become a schoolteacher,
and that was a profession he decidedly did not want. During the long winter
months he studied calculus on his own.

---

![](assets/APLSC_Ken_Iverson2.jpg){width=25%}

---

He was drafted in 1942, and during his service he took enough correspondence
courses to almost complete high school. After the military service he earned a
B.A. in both mathematics and physics from Queen’s University in Kingston
Ontario, and then an M.A. in physics from Harvard University. In 1954 he
completed a PhD under computer pioneer Howard Aiken, with a thesis titled
“Machine Solutions of Linear Differential Equations: Applications to a Dynamic
Economic Model”.

After completing his doctorate, Iverson joined the Harvard faculty to teach in
Aiken’s new automatic data processing program. He was there for one year as an
Instructor, and for five years as an Assistant Professor. He became
increasingly frustrated with the inadequacy of conventional mathematical
notation for expressing algorithms, so he began to invent his own.

In 1960 Iverson joined the new IBM Research Center in Yorktown Heights, New
York, on the advice of Frederick Brooks, who had been one of his teaching
fellows at Harvard and was now at IBM. The two collaborated on the continuing
development of the new notation. In 1962 Ken published the now-classic book “A
Programming Language” [^1], the title of which gave the name APL to the
notation which had up until then been informally called “Iverson’s notation”.

Iverson continued to work on the development of APL throughout his tenure at
IBM. In 1980 he left IBM and returned to Canada to work for I.P. Sharp
Associates, which had established an APL-based timesharing service.

---

![](assets/APLSC_Ken_Iverson3.jpg){width=25%}

---

In 1987 he “retired from paid employment” and turned his full attention to the
development of a more modern dialect of APL. APL was successfully being used
for commercial purposes, but Iverson wanted to develop a new simple executable
notation more suitable for teaching, which would be available at low cost. The
first implementation of this language, called J, was announced at the APL90
Users’ Conference.

Iverson’s ability to create such languages came from his “sheer enjoyment of
language and words,” recalls his daughter Janet Cramer. “He read dictionaries
like people read novels.” Iverson thought it was important that language, both
English and mathematics, communicate clearly and concisely.

With collaborators that included his son Eric, Iverson continued to work on
the development of J, and he continued to publish prolifically. On Saturday,
October 16, 2004 he suffered a stroke –while working on a J tutorial — and
died three days later on October 19, at the age of 83.

There are many stories about Ken Iverson. Here are a few:

> Ken didn’t get tenure at Harvard. He did his five years as an assistant
> professor and the Faculty decided not to put him up for promotion. I asked
> him what went wrong and he said, “Well, the Dean called me in and said, ‘the
> trouble is, you haven’t published anything but the one little book’”. The
> one little book later got [him] the Turing Award. I think that is a comment
> on the conventional mindset of promotion procedures rather than a comment on
> Ken; it’s a comment on academic procedure and on Harvard.
>
> -— Fred Brooks, A Celebration of Kenneth Iverson, 2004-11-30

> In an early talk Ken was explaining the advantages of tolerant comparison. A
> member of the audience asked incredulously, “Surely you don’t mean that when
> A=B and B=C, A may not equal C?” Without skipping a beat, Ken replied, “Any
> carpenter knows that!” and went on to the next question.
>
> -— Paul Berry

> In a social conversation with Ken, I said, “You know, Ken, you are my
> favorite language designer and Don Knuth is my favorite programmer.” And Ken
> said immediately, “What’s wrong with my programming?”
>
> -— Joey Tuttle, A Celebration of Kenneth Iverson, 2004-11-30

> In 1973 or 1974 Ken and I gave a talk at Kodak in Rochester to a group of 40
> to 50 programmers who were required to work in PL/I. In the question period
> a senior staff member said, “If I understand what you people are saying, you
> are suggesting that we should adopt a new way of thinking.” And Ken jumped
> up out of his chair and said, “Yes! That’s exactly what I am saying!”
>
> — Joey Tuttle, A Celebration of Kenneth Iverson, 2004-11-30

---

[![](https://img.youtube.com/vi/8kUQWuK1L4w/hqdefault.jpg)](https://youtu.be/8kUQWuK1L4w)

*The Origins of APL – 1974; a wonderful interview with the original developers of APL.*

---

[L. Breed, “How We Got to APL1130,” 10 May 2004.](assets/102636222.05.01.acc.pdf)

---

[^1]:  K. E. Iverson, A Programming Language, John Wiley and Sons, Inc., 1962.
[^2]:  [IBM, “APL Language,” March 1975.](https://archive.org/stream/bitsavers_ibmaplGC2675_6717824/GC26-3847-0_APL_Language_Mar75_djvu.txt)
[^3]:  [IBM, “APL\360 User’s Manual,” March 1970.](https://archive.org/details/bitsavers_ibmaplGH20ualMar70_7246596)
[^4]:  [IBM, Paul Berry, “APL\360 Primer – Student Text,” 1969.](https://archive.org/details/bitsavers_ibmaplC2019_13626230)
[^5]:  [L. M. Breed and R. H. Lathwell, “APL\360,” 1968.](https://archive.org/details/bitsavers_ibmapl360D_7227158)
[^6]:  L. M. Breed and R. H. Lathwell, “The Implementation of APL\360,” in ACM Symposium on Experimental Systems for Interactive Applied Mathematics, 1967.
[^7]:  A. D. Falkoff, K. E. Iverson and E. H. Sussenguth, “A Formal Description of SYSTEM/360,” IBM Systems Journal, vol. 3, no. 3, pp. 198-261, 1964.
[^8]:  P. C. Berry and J. R. Thorstensen, “Starmap,” 1978.
[^9]:  P. C. Berry and J. R. Thorstensen, “Starmap,” IBM Systems Development Division, 1975.
[^10]: IBM Systems Journal, vol. 30, no. 4, 1991.
[^11]: K. E. Iverson, “A Personal View of APL,” IBM Systems Journal, vol. 30, no. 4, 1991.
[^12]: A. J. Perlis, “In Praise of APL: A Language for Lyrical Programming,” SIAM News, June 1977.
[^13]: B. McCormick, “A Programming Language,” 2000,2002.
[^14]: E. W. Dijkstra, “How Do We Tell Truths That Might Hurt?”,” SIGPLAN Notices, vol. 17, no. 5, May 1982.
